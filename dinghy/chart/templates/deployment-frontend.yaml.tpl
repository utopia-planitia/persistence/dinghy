apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
spec:
  replicas: 2
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchExpressions:
                  - key: app
                    operator: In
                    values:
                      - frontend
              topologyKey: "kubernetes.io/hostname"
      containers:
        - name: frontend
          image: ghcr.io/damoon/dinghy/frontend:latest@sha256:6d0e18e76c272b867260043ce408bfbe3f958bfdc21cb2305d3048cc60dbf228
          env:
            - name: BACKEND_URL
              value: https://{{ .Values.domain }}/_
            - name: WEBSOCKET_URL
              value: wss://{{ .Values.domain }}/_/
          ports:
            - containerPort: 8000
          readinessProbe:
            httpGet:
              path: /
              port: 8000

apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend
spec:
  replicas: 2
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchExpressions:
                  - key: app
                    operator: In
                    values:
                      - backend
              topologyKey: "kubernetes.io/hostname"
      containers:
        - name: backend
          image: ghcr.io/damoon/dinghy/backend:latest@sha256:f9e656f50ba5d66b625d67053a7239f39cb8d1476d257158d4eaa7018e038087
          command:
            - ash
            - -c
            - |
              set -euo pipefail
          {{ if ne .Values.additionalCA "" }}
              apk add --udpate --no-cache curl
              curl --fail -L -o /usr/local/share/ca-certificates/fakelerootx1.crt {{ .Values.additionalCA }}
              update-ca-certificates
          {{ end }}
              /usr/local/bin/backend server \
              --s3-endpoint={{ .Values.s3endpoint }} \
              --s3-access-key-file=/secret/access_key \
              --s3-secret-key-file=/secret/secret_key \
              --s3-bucket=dinghy-storage \
              --frontend-url=https://{{ .Values.domain }}
          env:
            - name: JAEGER_AGENT_HOST
              value: jaeger.monitoring.svc
            - name: JAEGER_AGENT_PORT
              value: "6831"
          ports:
            - name: service
              containerPort: 8080
            - name: admin
              containerPort: 8090
          readinessProbe:
            httpGet:
              path: /healthz
              port: 8090
          volumeMounts:
            - name: secret
              mountPath: "/secret"
              readOnly: true
      volumes:
        - name: secret
          secret:
            secretName: dinghy-storage-bucket
            items:
            - key: AWS_ACCESS_KEY_ID
              path: access_key
            - key: AWS_SECRET_ACCESS_KEY
              path: secret_key

#!/bin/bash
set -euo pipefail

mkdir /workspace
cd /workspace

curl -X MKCOL -s http://webdav.webdav.svc/test_folder/

date --utc > file.txt
curl -XPUT -T file.txt http://webdav.webdav.svc/test_folder/

curl -o downloaded.txt http://webdav.webdav.svc/test_folder/file.txt
diff file.txt downloaded.txt

curl -X DELETE -s http://webdav.webdav.svc/test_folder/

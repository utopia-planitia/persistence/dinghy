# Webdav

A deployment of Dinghy
https://github.com/damoon/dinghy

## External Access

Dinghy is accessable via `http://webdav.{{ Cluster domain }}` from outside the cluster.

## CI Access

Dinghy is accessable via `http://webdav.webdav.svc.cluster.local` from within the cluster.

## Known limitations

Using minio as a gateway to another s3 location does not allow to use notifications.
